﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RadialTimer : MonoBehaviour 
{
    [SerializeField]
    public Image progressBar;
    [SerializeField]
    public Text progressText;

    public Timer timer = new Timer();

    void Awake()
    {
        timer.onUpdated = UpdateProgressBar;
    }

	// Update is called once per frame
    void Update()
    {
        timer.Update();
    }

    void UpdateProgressBar(float progress)
    {
        if(timer.time <= 0)
        {
            progressBar.fillAmount = 0;
            progressText.text = "0";
            return;
        }

        progressBar.fillAmount = progress / timer.time;
        progressText.text = "" + progress.ToString("#0.");;
    }

    public void SetColor(Color color)
    {
        progressBar.color = color;
    }
}
