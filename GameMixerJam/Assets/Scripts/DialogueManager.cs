﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class StringList
{
    public List<string> list;
}

public class DialogueManager : MonoBehaviour 
{
    ///  This should be made with Tuples
    ///  http://stackoverflow.com/questions/166089/what-is-c-sharp-analog-of-c-stdpair
    [SerializeField]
    StringList[] zeroAttacks;

    [SerializeField]
    StringList[] zeroDefenses;

    [SerializeField]
    StringList[] oneAttacks;

    [SerializeField]
    StringList[] oneDefenses;

    [SerializeField]
    List<string> excuses;

    [SerializeField]
    List<string> refuses;

    /// Singleton stuff 
    public static DialogueManager instance = null;
    void Awake()
    {
        if(instance == null)
            instance = this;
        else if(instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void ShowDialogue(Player attacker, Player defender, Food.Phase foodPhase, bool secondTime)
    {
        int bigRandom = (int) (Random.Range(0.0f, 1.0f) * 1000);

        //Debug.LogError("foodPhase == " + foodPhase);

        if(foodPhase == Food.Phase.None) /// Excuse Case
        {
            if(!secondTime)
            {
                attacker.dialogue.ShowDialogue(excuses[bigRandom % excuses.Count]);
                defender.dialogue.ShowDialogue("");
            }
            else /// Second Time
            {
                attacker.dialogue.ShowDialogue(refuses[bigRandom % excuses.Count]);
                defender.dialogue.ShowDialogue("");
            }
            return;
        }

        if(attacker.index == 0) /// zero case
        {
            int foodIndex = ((int) foodPhase) - 1;
            int rand = bigRandom % zeroAttacks[foodIndex].list.Count;
            attacker.dialogue.ShowDialogue(zeroAttacks[foodIndex].list[rand]);
            defender.dialogue.ShowDialogue(oneDefenses[foodIndex].list[rand]);
            return;
        }

        if(attacker.index == 1) /// one case
        {
            int foodIndex = ((int) foodPhase) - 1;
            int rand = bigRandom % oneAttacks[foodIndex].list.Count;
            attacker.dialogue.ShowDialogue(oneAttacks[foodIndex].list[rand]);
            defender.dialogue.ShowDialogue(zeroDefenses[foodIndex].list[rand]);
            return;
        }
    }
}
