﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Logger : MonoBehaviour 
{
    public static Logger instance = null;

    private List<string> events = new List<string>();
    public Transform contentPanel;
    public GameObject LogView;
    public SimpleObjectPool logItemsObjectPool;
    Text lastEventText = null;

    void Awake()
    {
        if(instance == null)
            instance = this;
        else if(instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if(Input.GetButtonDown("ToggleLog"))
        {
            LogView.SetActive(!LogView.activeInHierarchy);
        }
    }

    public void AddEvent(string eventString)
    {
        Debug.Log(eventString);
        events.Add(eventString);

        /// View related
        GameObject newLogEntry = logItemsObjectPool.GetObject();
        newLogEntry.transform.SetParent(contentPanel);

        lastEventText = newLogEntry.GetComponent<Text>();
        lastEventText.text = eventString;
    }

    public void AppendToLastEvent(string eventString)
    {
        events[events.Count - 1] += eventString;
        lastEventText.text = events[events.Count - 1];
        Debug.Log(eventString);
    }
}

