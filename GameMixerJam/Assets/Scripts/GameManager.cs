﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
    [SerializeField]
    List<Player> players;

    [SerializeField]
    GameObject[] playerPrefabs;

    [SerializeField]
    Transform[] playerMountPoints;

    [SerializeField]
    int turnCounter;

    [SerializeField]
    int playerCount = 2;

    [SerializeField]
    [Range(0.0f, 60.0f)]
    float turnTime = 10.0f;

    [SerializeField]
    RadialTimer radialTimer;

    bool skipped = false;

    /// Singleton stuff 
    public static GameManager instance = null;
    void Awake()
    {
        if(instance == null)
            instance = this;
        else if(instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

	// Use this for initialization
	void Start () 
    {
        Init();
        radialTimer.timer.time = turnTime;
        radialTimer.timer.onEnded = SkipTurn;
        radialTimer.timer.Restart();
	}

    private int lastTurn = -1;
	
	// Update is called once per frame
	void Update () 
    {
        int rest = turnCounter % playerCount;
        int turn = turnCounter / playerCount + rest;
        if(turn != lastTurn)
        {
            Logger.instance.AddEvent(">>> Starting Turn " + turn + ".");
            lastTurn = turn;
        }

        bool processed = ProcessInput();
        //turnTimer.Update(); // Radial timers update themselves
        if(processed || skipped)
            ProcessTurn();
	}

    void SkipTurn()
    {
        Logger.instance.AddEvent(players[turnCounter % playerCount].Name + " skipped the turn!");
        skipped = true;
    }

    bool ProcessInput()
    {
        int currentPlayer = turnCounter % playerCount;
        int targetPlayer = (turnCounter + 1) % playerCount; /// TODO Calculate this based on player input for more than 2 players
        bool poisonModifier = false;
        Food.Phase resultPhase = Food.Phase.None;

        if(currentPlayer == 0) /// Player 0 is playing
        {
            if(Input.GetButton("1PoisonModifier"))
            {
                poisonModifier = true;
            }

            if(Input.GetButtonDown("1CreateFood"))
            {
                resultPhase = players[targetPlayer].CreateFood(Food.Type.Food, poisonModifier);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " ordered " + (poisonModifier ? "poisoned " : "") + "food for " + players[targetPlayer].Name);
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], resultPhase, false);
                }
            }

            if(Input.GetButtonDown("1CreateDrink"))
            {
                resultPhase = players[targetPlayer].CreateFood(Food.Type.Drink, poisonModifier);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " ordered a " + (poisonModifier ? "poisoned " : "") + "drink for " + players[targetPlayer].Name);
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], resultPhase, false);
                }
            }

            if(Input.GetButtonDown("1ExcuseFood"))
            {
                resultPhase = players[currentPlayer].ExcuseFood(Food.Type.Food);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " refused to eat his food this turn.");
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], Food.Phase.None, false);
                }
            }

            if(Input.GetButtonDown("1ExcuseDrink"))
            {
                resultPhase = players[currentPlayer].ExcuseFood(Food.Type.Drink);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " refused to drink this turn.");
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], Food.Phase.None, false);
                }
            }
        }

        if(currentPlayer == 1) /// Player 0 is playing
        {
            if(Input.GetButton("2PoisonModifier"))
            {
                poisonModifier = true;
            }

            if(Input.GetButtonDown("2CreateFood"))
            {
                resultPhase = players[targetPlayer].CreateFood(Food.Type.Food, poisonModifier);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " ordered " + (poisonModifier ? "poisoned " : "") + "food for " + players[targetPlayer].Name);
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], resultPhase, false);
                }
            }

            if(Input.GetButtonDown("2CreateDrink"))
            {
                resultPhase = players[targetPlayer].CreateFood(Food.Type.Drink, poisonModifier);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " ordered a " + (poisonModifier ? "poisoned " : "") + "drink for " + players[targetPlayer].Name);
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], resultPhase, false);
                }
            }

            if(Input.GetButtonDown("2ExcuseFood"))
            {
                resultPhase =  players[currentPlayer].ExcuseFood(Food.Type.Food);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " refused to eat his food this turn.");
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], Food.Phase.None, false);
                }
            }

            if(Input.GetButtonDown("2ExcuseDrink"))
            {
                resultPhase =  players[currentPlayer].ExcuseFood(Food.Type.Drink);
                if(resultPhase != Food.Phase.None)
                {
                    Logger.instance.AddEvent(players[currentPlayer].Name + " refused to drink this turn.");
                    DialogueManager.instance.ShowDialogue(players[currentPlayer], players[targetPlayer], Food.Phase.None, false);
                }
            }
        }

        if(resultPhase != Food.Phase.None)
            return true;

        return false;
    }

    public void Init()
    {
        CreatePlayers();
    }

    void CreatePlayers()
    {
        players = new List<Player>();
        for(int i = 0; i < playerCount; ++i)
        {
            GameObject playerObj = Instantiate(playerPrefabs[i]) as GameObject;
            Player player = playerObj.GetComponent<Player>();
            player.Init(i, playerMountPoints[i].position);

            players.Add(player);
        }
    }

    public void ProcessTurn()
    {
        players[turnCounter % playerCount].ProcessTurn();

        radialTimer.timer.Restart();
        skipped = false;
        ++turnCounter;

        if(turnCounter % playerCount == 0)
            radialTimer.SetColor(new Color(74.0f / 255.0f, 180.0f / 255.0f, 255.0f / 255.0f, 1.0f));
        else
            radialTimer.SetColor(new Color(255.0f / 255.0f, 81.0f / 255.0f, 61.0f / 255.0f, 1.0f));
            
    }

    #region Logger

    private List<string> events = new List<string>();

    public void AddEvent(string eventString)
    {
        Debug.Log(eventString);
        events.Add(eventString);
    }

    #endregion Logger
}
