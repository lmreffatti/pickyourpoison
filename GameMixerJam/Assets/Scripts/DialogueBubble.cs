﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueBubble : MonoBehaviour 
{
    Text text;

    void Awake()
    {
        text = GetComponent<Text>();
    }

    public void ShowDialogue(string phrase)
    {
        text.text = phrase;
    }
}
