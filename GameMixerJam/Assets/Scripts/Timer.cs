﻿using UnityEngine;
using System.Collections;

public class Timer
{
    public float time = 60.0f;
    public float remainingTime = 0;
    private bool running = false;

    public delegate void OnEndedDelegate();
    public OnEndedDelegate onEnded;

    public delegate void OnUpdatedDelegate(float remainingTime);
    public OnUpdatedDelegate onUpdated;

	// Update is called once per frame
    public void Update()
    {
        remainingTime -= Time.deltaTime;

        if(onUpdated != null)
            onUpdated(remainingTime);

        if(remainingTime <= 0.0f && onEnded != null)
            onEnded();
    }

    public void Start()
    {
        running = true;
    }

    public void Restart()
    {
        remainingTime = time;
        running = true;
    }

    public void Stop()
    {
        running = false;
    }
}
