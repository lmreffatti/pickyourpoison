﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour 
{
    [Range(0, 3)]
    public int index = 0;

    [SerializeField]
    GameObject foodPrefab;

    [SerializeField]
    public DialogueBubble dialogue;

    [SerializeField]
    private string name = "";
    public string Name
    {
        get
        {
            if(name == null || name == "")
                return ("Agent " + index);

            return this.name;
        }
        set
        {
            this.name = value;
        }
    }

    [SerializeField]
    [Range(-1, 10)]
    int hp = 1;

    [SerializeField]
    [Range(0, 10)]
    int excuses = 5;

    [SerializeField]
    [Range(0, 10)]
    int poisons = 5;

    [SerializeField]
    [Range(-1, 10)]
    int poisonTurns = 0;

    [SerializeField]
    bool poisoned = false;

    [SerializeField]
    Food[] foods = new Food[(int) Food.Type.Count];

    [SerializeField]
    Food.Phase phase = Food.Phase.Starter;

    [SerializeField]
    Transform[] mountPoints;

    public void Init(int index, Vector3 pos, string name = "")
    {
        gameObject.name = "Player " + index;
        this.index = index;
        transform.position = pos;
        this.name = name;
    }

    /*
     *	State processing
	 *	Updates player status using current food status.
	 *		Calls food to update itself (all effects on player should be calculated by now)
     */
    public void ProcessTurn()
    {
        foreach(Food food in foods)
        {
            if(food == null)
                continue;

            if(food.deliveryDelay <= 0 && food.timeToEat <= 0 && food.poisoned)
            {
                this.poisoned = food.poisoned;
                if(poisonTurns > food.poisonDelay)
                {
                    poisonTurns = food.poisonDelay;
                }
                Logger.instance.AddEvent(this.Name + " is poisoned, after eating a " + food.gameObject.name);
            }

            if(poisoned)
            {
                --poisonTurns;
                if(poisonTurns < 0)
                {
                    Logger.instance.AddEvent(this.Name + " died of food poisoning from " + food.gameObject.name);
                    Destroy(gameObject);
                }
            }

            food.ProcessTurn();
        }
    }

    public Food.Phase CreateFood(Food.Type type, bool poisoned)
    {
        int slotIndex = ((int) type) - 1;

        if(type == Food.Type.Food && this.foods[slotIndex] == null)
        {
            GameObject foodObj = Instantiate(foodPrefab) as GameObject;
            Food food = foodObj.GetComponent<Food>();

            food.Init(Food.Type.Food, (Food.Phase) (int) phase +1, Food.Skintype.None, poisoned, 0, mountPoints[slotIndex].position);
            foodObj.transform.parent = this.transform;

            foods[slotIndex] = food;

            Logger.instance.AppendToLastEvent(" It's a nice " + food.gameObject.name);

            Food.Phase oldPhase = phase;
            phase = (Food.Phase) ((int) phase + 1);

            return phase;
        }

        if(type == Food.Type.Drink && this.foods[slotIndex] == null)
        {
            GameObject foodObj = Instantiate(foodPrefab) as GameObject;
            Food food = foodObj.GetComponent<Food>();

            food.Init(Food.Type.Drink, Food.Phase.Drink, Food.Skintype.None, poisoned, 0, mountPoints[slotIndex].position);
            foodObj.transform.parent = this.transform;

            foods[slotIndex] = food;

            Logger.instance.AppendToLastEvent(" It's a nice " + food.gameObject.name);

            return Food.Phase.Drink;
        }

        return Food.Phase.None;
    }

    public Food.Phase ExcuseFood(Food.Type type)
    {
        int slotIndex = ((int) type) - 1;

        if(excuses > 0 && foods[slotIndex] != null)
        {
            --excuses;
            foods[slotIndex].Excuse();
            return phase;
        }

        return Food.Phase.None;
    }
}
