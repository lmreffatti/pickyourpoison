﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour 
{
    public enum Type
    {
        None = 0,
        Food = 1,
        Drink = 2,
        Count = 2
    }
    Type type;

    public enum Phase
    {
        None = 0,
        Drink = 1,
        Starter = 2,
        Main = 3,
        Dessert = 4
    }
    Phase phase;

    public enum Skintype
    {
        None = 0,
        Martini = 1,
        Wine = 2,
        Whisky = 3
    }
    Skintype skintype;

    [SerializeField]
    [Range(0, 10)]
    public int timeToEat = 3;

    [SerializeField]
    [Range(0, 10)]
    public int excused = 0;

    [SerializeField]
    public bool poisoned = false;

    [SerializeField]
    [Range(0, 10)]
    public int poisonDelay = 0;

    [SerializeField]
    [Range(0, 10)]
    public int deliveryDelay = 1;

    [SerializeField]
    Sprite[] sprites;

    [SerializeField]
    SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Init(Type type, Phase phase, Skintype skintype, bool poisoned, int poisonDelay, Vector3 pos)
    {
        this.type = type;
        this.skintype = skintype;
        spriteRenderer.sprite = sprites[((int)phase) -1];

        this.poisoned = poisoned;
        this.poisonDelay = poisonDelay;

        this.transform.position = pos;

        this.phase = phase;
        switch(phase)
        {
            case Phase.Drink:
                timeToEat = 2;
                deliveryDelay = 0;
                gameObject.name = "Drink " + (int) phase + (int) skintype + poisoned;
            break;
            case Phase.Starter:
                timeToEat = 3;
                deliveryDelay = 1;
                gameObject.name = "Starter " + (int) phase + (int) skintype + poisoned;
            break;
            case Phase.Main:
                timeToEat = 5;
                deliveryDelay = 1;
                gameObject.name = "Main " + (int) phase + (int) skintype + poisoned;
            break;
            case Phase.Dessert:
            default:
                timeToEat = 3;
                deliveryDelay = 1;
                gameObject.name = "Dessert " + (int) phase + (int) skintype + poisoned;
                this.poisoned = true;
            break;
        }
    }

    /*
     *	State processing
     *	Updates player status using current food status.
     *		Calls food to update itself (all effects on player should be calculated by now)
     */
    public void ProcessTurn()
    {
        --deliveryDelay;
        if(deliveryDelay >= 0)
            return;

        --timeToEat;
        if(timeToEat >= 0)
            return;

        Logger.instance.AddEvent((type == Type.Drink ? "Drink " : "Food ") + gameObject.name + " was consumed.");
        Destroy(gameObject);
    }

    public void Excuse()
    {
        ++excused;
        if(excused >= 2 && gameObject != null)
        {
            Logger.instance.AddEvent((type == Type.Drink ? "Drink " : "Food ") + gameObject.name + "was excused two times and was removed.");
            Destroy(gameObject);
        }

        if(deliveryDelay < 0 )
        {
            ++timeToEat;
            Logger.instance.AppendToLastEvent((type == Type.Drink ? "Drink " : "Food ") + gameObject.name + " has more " + (timeToEat + 1) + " turns before being consumed.");
        }
    }
}
